# coding: utf-8
from __future__ import unicode_literals
from django.core.exceptions import ValidationError
from django.db import models
import re

class Answer(models.Model):
    name = models.CharField("Enter your name", max_length=200)
    limit_1 = 5
    MK1_CHOICES = [
        ('', u'Выберете теоретический мастер-класс'.upper()),
        ('mk1', 'name1'.upper()),
        ('mk2', 'name2'.upper()),
        ('bumper', 'bumper'.upper()),
    ]
    mk1 = models.CharField(max_length=3, choices=MK1_CHOICES)

    limit_2 = 5
    MK2_CHOICES = [
        ('', u'Выберете практический мастер-класс'.upper()),
        ('mk1', 'name1'.upper()),
        ('mk2', 'name2'.upper()),
        ('mk3', 'name3'.upper()),
    ]

    mk2 = models.CharField(max_length=3, choices=MK2_CHOICES)

    limit_3 = 5
    MK3_CHOICES = [
        ('', u'Выберете практический мастер-класс'.upper()),
        ('mk1', 'name1'.upper()),
        ('mk2', 'name2'.upper()),
        ('mk3', 'name3'.upper()),
    ]
    mk3 = models.CharField(max_length=3, choices=MK3_CHOICES)

    limit_4 = 5
    MK4_CHOICES = [
        ('', u'Выберете практический мастер-класс'.upper()),
        ('mk1', 'name1'.upper()),
        ('mk2', 'name2'.upper()),
        ('mk3', 'name3'.upper()),
    ]
    mk4 = models.CharField(max_length=3, choices=MK4_CHOICES)


def validate_phone(value):
    test = re.compile('^\+?[0-9()-]+$')
    if test.match(value):
        raise ValidationError(_('%(value)s is not an phone number'), params={'value': value})


class Comand(models.Model):
    tel = models.CharField('Mobile phone', max_length=200, validators=[validate_phone])
    name1 = models.CharField('name1', max_length=200)
    name2 = models.CharField('name2', max_length=200)
    name3 = models.CharField('name3', max_length=200)
    name4 = models.CharField('name4', max_length=200)
    name5 = models.CharField('name5', max_length=200)
    name6 = models.CharField('name6', max_length=200)

