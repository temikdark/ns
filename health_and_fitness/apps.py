from __future__ import unicode_literals

from django.apps import AppConfig


class HealthAndFitnessConfig(AppConfig):
    name = 'health_and_fitness'
