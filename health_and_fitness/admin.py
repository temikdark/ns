from django.contrib import admin
from models import Answer, Comand
# Register your models here.


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('name', 'mk1', 'mk2')


class ComandAdmin(admin.ModelAdmin):
    list_display = ('tel', 'name1')

admin.site.register(Answer, AnswerAdmin)
admin.site.register(Comand, ComandAdmin)
