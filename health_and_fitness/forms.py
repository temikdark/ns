from django.forms import ModelForm, CharField
from models import Answer, Comand


class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['name', 'mk1', 'mk2']

    def __init__(self, *args, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'placeholder': 'Enter your name'.upper()})
        self.fields['name'].widget.attrs.update({'id': 'input_name'})
        self.fields['mk1'].widget.attrs.update({'id': 'input_mk1', 'onchange':'control_bumperball()'})
        self.fields['mk2'].widget.attrs.update({'id': 'input_mk2'})

class ComandForm(ModelForm):
    class Meta:
        model = Comand
        fields = ['tel', 'name1', 'name2', 'name3', 'name4', 'name5', 'name6']

    def __init__(self, *args, **kwargs):
        super(ComandForm, self).__init__(*args, **kwargs)
        self.fields['name1'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['name2'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['name3'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['name4'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['name5'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['name6'].widget.attrs.update({'placeholder': 'Enter name'.upper()})
        self.fields['tel'].widget.attrs.update({'placeholder': '89031234567'})