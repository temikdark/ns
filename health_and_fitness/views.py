from django.shortcuts import render
import re
from django.core.exceptions import ValidationError

from forms import AnswerForm, ComandForm
from models import Answer, Comand
# Create your views here.


def index(request):
    return render(request, 'index.html')


def run(request):
    errors = False
    form = ''
    if request.method != 'POST':
        form = ComandForm()
    else:
        try:
            test = re.compile('^\+?[ 0-9()-]+$')
            form = ComandForm(request.POST)
            if not test.match(request.POST['tel']):
                raise ValidationError('%(value)s is not an phone number', params={'value': request.POST['tel']})

            com = Comand(tel=request.POST['tel'],
                         name1=request.POST['name1'],
                         name2=request.POST['name2'],
                         name3=request.POST['name3'],
                         name4=request.POST['name4'],
                         name5=request.POST['name5'],
                         name6=request.POST['name6'])
            com.clean_fields()
            com.save()

        except ValidationError as e:
            errors = True
    return render(request, 'run.html', {'registered': not errors and request.method == 'POST',
                                        'form': form,
                                        'errors': errors})


def masters(request):
    form = AnswerForm()
    mk1 = False
    mk2 = False
    is_name = False
    if request.method != 'POST' or len(request.POST) < 2:
        return render(request, 'masters.html', {'registered': False,
                                                'isname': is_name,
                                                'mk1': False,
                                                'mk2': False,
                                                'form': form})
    if not request.POST['name']:
        is_name = True
    if Answer.objects.filter(mk1=request.POST['mk1']).count() > Answer.limit_1 \
            or request.POST['mk1'] == Answer.MK1_CHOICES[0][0]:
        mk1 = True
    if Answer.objects.filter(mk2=request.POST['mk2']).count() > Answer.limit_2 \
            or request.POST['mk2'] == Answer.MK2_CHOICES[0][0] and request.POST['mk1'] != 'bumper':
        mk2 = True

    if mk1 or mk2 or is_name:
        return render(request, 'masters.html', {'registered': False,
                                                'isname': is_name,
                                                'mk1': mk1,
                                                'mk2': mk2,
                                                'form': form})
    Answer.objects.create(name=request.POST['name'],
                          mk1=request.POST['mk1'],
                          mk2=request.POST['mk2'],
                          )
    return render(request, 'masters.html', {'registered': True})
